//**************************************************************************/
// DESCRIPTION: Logger. Insert information with timestamp and decoding error codes Windows NT
// AUTHOR: Falkovski Alexander
// (C)2014
//***************************************************************************/

#pragma once

#include <fstream>
#include <string>

//Logger interface
class ILogger
{
public:
    virtual ~ILogger(){}
    virtual size_t append(std::string const &msg) = 0;
    virtual size_t append(std::wstring const &msg) = 0;
    virtual size_t append(std::string const &msg, size_t errorCode) = 0;
    virtual size_t append(std::wstring const &msg, size_t errorCode) = 0;
};

//Base class for use logger in child class
class ILoggable
{
protected:
    static ILogger * ILog;
    ILoggable(){ if(ILog == NULL) throw std::exception("ILogger not init"); }
public:
    virtual ~ILoggable(){}
    static void SetILogger(ILogger * log) { if(ILog == NULL) ILog = log; }
};

class Logger : public ILogger
{
protected:
    //base methods
    std::wofstream ofd;
    std::wstring LogFileName;

    bool is_open;
    static const wchar_t BOM = L'\xFEFF';
    
    static Logger * selfObject;
    Logger(void);
    Logger(std::wstring const &);
    Logger(Logger const &);
    Logger & operator=(Logger const &);
    virtual ~Logger(void);
    static inline Logger * CreateObject(std::wstring const &LogFile)
    {
        if(selfObject == NULL)
        {
            static Logger _self(LogFile);
            selfObject = &_self;
        }
        return selfObject;
    }

    inline size_t WriteFile(std::wstring const &); 
    
    //util methods
    void * mtx;
    bool isMultiThreadObject;
    void lock(bool force = false);
    void unlock(bool force = false);
    unsigned long MainThreadId;
    void CheckThread();

    inline bool FileExistsW(std::wstring const &);
    inline std::wstring Now();
    inline std::string GetMessageErrorA(size_t ErrorCode);
    inline std::wstring GetMessageErrorW(size_t ErrorCode);
public:
    static Logger * Instance(std::wstring const &LogFile) { return CreateObject(LogFile); }
    static Logger * Instance(std::string const &LogFile) { return CreateObject(StrToWStr(LogFile)); }

    Logger * Open();
    void Close();
    bool IsOpen() { return is_open; }

    size_t append(std::string const &msg); 
    size_t append(std::wstring const &msg);
    size_t append(std::string const &msg, size_t errorCode);
    size_t append(std::wstring const &msg, size_t errorCode);

    static std::wstring StrToWStr(std::string const &);
    static std::string WStrToStr(std::wstring const &);
};